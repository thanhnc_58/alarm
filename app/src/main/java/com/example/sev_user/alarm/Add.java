package com.example.sev_user.alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Set;

public class Add extends PreferenceActivity {

    private TimePicker timePicker;
    private SharedPreferences sharedPref;
    private PendingIntent pendingIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        addPreferencesFromResource(R.xml.preference);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        sharedPref = getPreferenceScreen().getSharedPreferences();

        ((ImageButton) findViewById(R.id.btn_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        ((ImageButton) findViewById(R.id.btn_save)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                String title = sharedPref.getString("title", null);
                int volume = sharedPref.getInt("volume", 0);
                Set<String> repeat = sharedPref.getStringSet("repeat", null);
                String[] repeatArray = repeat.toArray(new String[repeat.size()]);
                String ringtone = sharedPref.getString("ringtone", "DEFAULT_RINGTONE_URI");
                Alarm alarm =  new Alarm(timePicker.getCurrentHour(), timePicker.getCurrentMinute(), volume, repeatArray, title, ringtone);
                //playSound(Add.this, Uri.parse(ringtone));
                returnIntent.putExtra("newAlarm", alarm);      /*
                try {
                    FileOutputStream fos = getApplicationContext().openFileOutput("data.txt", Context.MODE_PRIVATE);
                    ObjectOutputStream os = new ObjectOutputStream(fos);
                    os.writeObject(alarm);
                    os.close();
                    fos.close();
                }
                catch (Exception e) {
                    Log.e("Alarm", "Write data to File error", e);
                }*/
                String result;
                int id;
                if ((id = (int) MainActivity.db.insertAlarm(alarm)) > - 1) {
                    alarm.setId(id);
                    result = "Set alarm successfully";
                }
                else {
                    result = "Failed";
                }
                (Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT)).show();
                setResult(Activity.RESULT_OK, returnIntent);
                MainActivity.alarmList.add(alarm);
                MainActivity.adapter.notifyDataSetChanged();
//                MainActivity.sortDecrease();
                setAlarm(alarm.getHour(), alarm.getMinute());
                finish();
            }
        });

        //timePicker.setIs24HourView(true);
    }
    public void setAlarm(int hour, int minute) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent i2 = new Intent(Add.this, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getActivity(Add.this, 0, i2, 0);
        pendingIntent = PendingIntent.getBroadcast(Add.this, 0, i2, 0);
        Calendar calNow = Calendar.getInstance();
        Calendar calSet = (Calendar) calNow.clone();
        calSet.set(Calendar.HOUR_OF_DAY, hour);
        calSet.set(Calendar.MINUTE, minute);
        calSet.set(Calendar.SECOND, 0);
        calSet.set(Calendar.MILLISECOND, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(calSet.getTimeInMillis(), pi),
                    pendingIntent);
        };
        //Toast.makeText(MainActivity.this, "Debug here", Toast.LENGTH_SHORT).show();
    }

    public void cancelAlarm(){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        Toast.makeText(Add.this, "sdffsd", Toast.LENGTH_SHORT).show();
    }
    private void playSound(Context context, Uri alert) {
        MediaPlayer mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
            System.out.println("OOPS");
        }
    }

}
