package com.example.sev_user.alarm;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by sev_user on 8/8/2016.
 */
public class Alarm implements Serializable{
    private int hour, minute, mode, volume;
    private String[] repeat;
    private String title, ringtone;
    private boolean active;
    private int id;


    public Alarm() {
        this(0, 0);
    }

    public Alarm(int h, int m) {
        this(h, m, 100, null, null, null);
    }

    public Alarm(int h, int m, int vol, String[] rep, String tit, String ringt) {
        hour = h;
        minute = m;
        volume = vol;
        repeat = rep;
        title = tit;
        ringtone = ringt;
        active = true;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setActive(boolean acti) {
        active = acti;
    }

    public void setMode(int mod) {
        mode = mod;
    }

    public void setHour(int h) {
        hour = h;
    }

    public void setMinute(int m) {
        minute = m;
    }

    public void setVolume(int vol) {
        volume = vol;
    }

    public void setRepeat(String[] rep) {
        repeat = rep;
    }

    public void setRingtone(String ringt) {
        ringtone = ringt;
    }

    public int getMode() {
        return mode;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getVolume() {
        return volume;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getRingtone() {
        return ringtone;
    }

    public boolean isActive() {
        return active;
    }

    public String[] getRepeat() {
        return repeat;
    }

    public String toString() {
        if (hour < 10 ) {
            if (minute < 10)return "0" + hour + " : 0" + minute;
            else return "0" + hour + " : " + minute;
        }
        else {
            if (minute < 10) return hour + " : 0" + minute;
            else return hour + " : " + minute;
        }

    }

    public int compareTo(Alarm o2) {
        int d1 = hour * 60 + minute + Boolean.valueOf(active).compareTo(false) * 100;
        int d2 = o2.hour + o2.minute + Boolean.valueOf(o2.active).compareTo(false) * 100;
        if (d1 > d2) return 1;
        if (d1 == d2) return 0;
        if (d1 < d2) return -1;
        return 0;
    }
}
