package com.example.sev_user.alarm;

import java.sql.Time;
import java.util.ArrayList;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class AlarmAdapter extends ArrayAdapter<Alarm>
{
    Activity context = null;
    ArrayList<Alarm> alarms = null;
    int layoutId;
    AlarmDB db;
    Add add = new Add();

    public AlarmAdapter(Activity context,
                          int layoutId,
                          ArrayList<Alarm> arr){
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.alarms = arr;
    }

    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        db = new AlarmDB(getContext());
        final Alarm alarm = alarms.get(position);
        final int id = alarm.getId();
        LayoutInflater inflater =
                context.getLayoutInflater();
        convertView = inflater.inflate(layoutId, null);
        if(alarms.size() > 0 && position >= 0)
        {
            Time time = new Time(1,1,1);
            final TextView timeDisplay = (TextView)
                    convertView.findViewById(R.id.time);
            final TextView titleDispaly = (TextView) convertView.findViewById(R.id.title);
            timeDisplay.setText(alarm.toString());
            titleDispaly.setText(alarm.getTitle());
            final Switch activeSwitch = (Switch)
                    convertView.findViewById(R.id.active);
            activeSwitch.setChecked(alarm.isActive());
            activeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    alarm.setActive(isChecked);
                    db.updateAlarm(id, alarm);
                }
            });

            final ImageButton deleteButton = (ImageButton) convertView.findViewById(R.id.delete);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    add.cancelAlarm();
                    db.deleteAlarm(id);
                    alarms.remove(position);
                    AlarmAdapter.this.notifyDataSetChanged();

                }
            });

        }
        return convertView;
    }
}