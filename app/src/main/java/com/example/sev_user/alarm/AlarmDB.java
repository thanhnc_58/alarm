package com.example.sev_user.alarm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class AlarmDB extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "AlarmApp.db";
    public static final String TABLE_NAME = "alarms";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_HOUR = "hour";
    public static final String COLUMN_MINUTE = "minute";
    public static final String COLUMN_REPEAT = "repeat";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_VOLUME = "volume";
    public static final String COLUMN_RINGTONE = "ringtone";
    public static final String COLUMN_ACTIVE = "active";

    public AlarmDB(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table alarms " +
                    "(id integer primary key, hour integer, minute integer, volume integer, title text, ringtone text, repeat text, active numeric)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alarms");
        onCreate(db);
    }

    public long insertAlarm  (Alarm alarm)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("hour", alarm.getHour());
        contentValues.put("minute", alarm .getMinute());
        contentValues.put("volume", alarm.getVolume());
        contentValues.put("title", alarm.getTitle());
        contentValues.put("ringtone", alarm.getRingtone());
        contentValues.put("repeat", convertArrayToString(alarm.getRepeat()));
        contentValues.put("active", alarm.isActive());
        return db.insert("alarms", null, contentValues);
    }

    public Cursor getData(long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from alarms where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

    public boolean updateAlarm (Integer id, Alarm alarm)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("hour", alarm.getHour());
        contentValues.put("minute", alarm .getMinute());
        contentValues.put("volume", alarm.getVolume());
        contentValues.put("title", alarm.getTitle());
        contentValues.put("ringtone", alarm.getRingtone());
        contentValues.put("repeat", convertArrayToString(alarm.getRepeat()));
        contentValues.put("active", alarm.isActive());
        db.update(TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteAlarm (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<Alarm> getAllAlarms()
    {
        ArrayList<Alarm> array_list = new ArrayList<Alarm>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from alarms", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int volume = res.getInt(res.getColumnIndex(COLUMN_VOLUME));
            String title = res.getString(res.getColumnIndex(COLUMN_TITLE));
            String ringtone = res.getString(res.getColumnIndex(COLUMN_RINGTONE));
            String[] repeat = convertStringToArray(res.getString(res.getColumnIndex(COLUMN_REPEAT)));
            Alarm alarm = new Alarm(res.getInt(res.getColumnIndex(COLUMN_HOUR)), res.getInt(res.getColumnIndex(COLUMN_MINUTE)), volume, repeat, title, ringtone);
            alarm.setActive(res.getInt(res.getColumnIndex(COLUMN_ACTIVE)) > 0);
            alarm.setId(res.getInt(res.getColumnIndex(COLUMN_ID)));
            array_list.add(alarm);
            res.moveToNext();
        }
        return array_list;
    }

    private final static String strSeparator = " ";

    public static String convertArrayToString(String[] array){
        String str = "";
        for (int i = 0;i<array.length; i++) {
            str = str+array[i];
            // Do not append comma at the end of last element
            if(i<array.length-1){
                str = str + strSeparator;
            }
        }
        return str;
    }
    public static String[] convertStringToArray(String str){
        String[] arr = str.split(strSeparator);
        return arr;
    }
}