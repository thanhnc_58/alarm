package com.example.sev_user.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by sev_user on 8/10/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentService = new Intent(context, TimeUp.class);
        //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Toast.makeText(context, "Receive completed", Toast.LENGTH_SHORT).show();
        context.startService(intentService);
    }
}
