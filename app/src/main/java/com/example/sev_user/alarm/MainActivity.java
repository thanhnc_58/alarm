package com.example.sev_user.alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    public static ArrayList<Alarm> alarmList;
    public static ArrayAdapter<Alarm> adapter;
    public static AlarmDB db;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        View view = findViewById(R.id.layout);
        view.setBackgroundColor(Color.parseColor("#f2f2f2"));

        db = new AlarmDB(this);
        alarmList = db.getAllAlarms();
        adapter = new AlarmAdapter(this, R.layout.item_layout, alarmList);
        lv = (ListView) findViewById(R.id.lv_alarms);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long rowid) {
                int id = position + 1;

                if(id > 0){
                    Cursor rs = db.getData(id);
                    rs.moveToFirst();
                    int hour = rs.getInt(rs.getColumnIndex(AlarmDB.COLUMN_HOUR));
                    int minute = rs.getInt(rs.getColumnIndex(AlarmDB.COLUMN_MINUTE));
                    String ringtone = rs.getString(rs.getColumnIndex(AlarmDB.COLUMN_RINGTONE));
                    String[] repeat = AlarmDB.convertStringToArray(rs.getString(rs.getColumnIndex(AlarmDB.COLUMN_REPEAT)));
                    boolean active  = rs.getInt(rs.getColumnIndex(AlarmDB.COLUMN_ACTIVE)) > 0;
                    //Log.i("Tung", Boolean.toString(active));

                    if (!rs.isClosed())
                    {
                        rs.close();
                    }
                    (Toast.makeText(getApplicationContext(), "Alarm " + hour + ":" + minute, Toast.LENGTH_SHORT)).show();
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Add.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){

            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
}
