package com.example.sev_user.alarm;

import android.app.DialogFragment;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.*;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import java.util.jar.Attributes;

/**
 * Created by sev_user on 8/6/2016.
 */
public class SeekbarPreference extends Preference implements SeekBar.OnSeekBarChangeListener {

    private int progress;
    private SeekBar seekbar;

    public SeekbarPreference(Context context) {
        this(context, null, 0);
    }
    public SeekbarPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekbarPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayoutResource(R.layout.seekbar_preference);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        seekbar = (SeekBar) view.findViewById(R.id.seekbar);
        seekbar.setProgress(progress);
        seekbar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
        if (!fromUser) return;
        setValue(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // not used
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // not used
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setValue(restoreValue ? getPersistedInt(progress) : (Integer) defaultValue);
    }

    public void setValue(int value) {
        if (shouldPersist()) {
            persistInt(value);
        }
        if (value != progress) {
            progress = value;
            notifyChanged();
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, 0);
    }
}