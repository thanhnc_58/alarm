package com.example.sev_user.alarm;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Toast;

import java.io.IOException;


public class TimeUp extends Service  {
    private static final String ACTION_PLAY = "com.example.action.PLAY";
    private MediaPlayer mediaPlayer;


    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent i = new Intent(getApplicationContext(), AlertActivity.class);
        i.setAction(Intent.ACTION_VIEW);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
//        mMediaPlayer = MediaPlayer.create(this, tone);
//        mMediaPlayer.setOnPreparedListener(this);
//        mMediaPlayer.prepareAsync();
//        try {
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
//            ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
//            ringtone.play();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        mediaPlayer = new MediaPlayer();
        try{
            mediaPlayer.setDataSource(this, Uri.parse("content://media/internal/audio/media/13"));
            final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            int valuess = 15;//range(0-15)
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, valuess, 0);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        Toast.makeText(TimeUp.this, "This is in onBind", Toast.LENGTH_SHORT).show();
        return null;
    }

    
    @Override
    public void onDestroy() {
        //ringtone.stop();
        mediaPlayer.stop();
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }



}